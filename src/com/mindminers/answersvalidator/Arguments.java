package com.mindminers.answersvalidator;

public class Arguments {
    private String usersFilePath;
    private String citiesFilePath;
    private String surveyAnswersFilePath;
    private String optionsFilePath;
    private String answersFilePath;
    private String questionsFilePath;

    private Arguments() { }

    public String getUsersFilePath() {
        return usersFilePath;
    }

    public String getCitiesFilePath() {
        return citiesFilePath;
    }

    public String getSurveyAnswersFilePath() {
        return surveyAnswersFilePath;
    }

    public String getOptionsFilePath() {
        return optionsFilePath;
    }

    public String getAnswersFilePath() {
        return answersFilePath;
    }

    public String getQuestionsFilePath() {
        return questionsFilePath;
    }

    public static class Builder {
        private Arguments arguments;

        private Builder() {
            arguments = new Arguments();
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder withUsersFilePath(String usersFilePath) {
            arguments.usersFilePath = usersFilePath;
            return this;
        }

        public Builder withCitiesFilePath(String citiesFilePath) {
            arguments.citiesFilePath = citiesFilePath;
            return this;
        }

        public Builder withSurveyFilePath(String surveyFilePath) {
            arguments.surveyAnswersFilePath = surveyFilePath;
            return this;
        }

        public Builder withOptionsFilePath(String optionsFilePath) {
            arguments.optionsFilePath = optionsFilePath;
            return this;
        }

        public Builder withAnswersFilePath(String answersFilePath) {
            arguments.answersFilePath = answersFilePath;
            return this;
        }

        public Builder withQuestionsFilePath(String questionsFilePath) {
            arguments.questionsFilePath = questionsFilePath;
            return this;
        }

        public Arguments build() {
            return arguments;
        }
    }
}
