package com.mindminers.answersvalidator.models;

public class User {
    public int id;
    public String firstName;
    public String lastName;
    public String email;
    public Gender gender;
    public int age;
    public int cityId;
}
