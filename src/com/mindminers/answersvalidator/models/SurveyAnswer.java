package com.mindminers.answersvalidator.models;

public class SurveyAnswer {
    public int questionId;
    public int answerId;
    public int optionId;
    public int userId;
}
