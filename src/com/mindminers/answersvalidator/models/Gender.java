package com.mindminers.answersvalidator.models;

public enum Gender {
    Female,
    Male
}
