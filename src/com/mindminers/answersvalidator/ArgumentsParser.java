package com.mindminers.answersvalidator;

public class ArgumentsParser {
    public static Arguments parse(String[] args) {
        Arguments.Builder builder = Arguments.Builder.newBuilder();

        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("-u"))
                builder.withUsersFilePath(args[i + 1]);

            if(args[i].equals("-c"))
                builder.withCitiesFilePath(args[i + 1]);

            if(args[i].equals("-s"))
                builder.withSurveyFilePath(args[i + 1]);

            if(args[i].equals("-o"))
                builder.withOptionsFilePath(args[i + 1]);

            if(args[i].equals("-a"))
                builder.withAnswersFilePath(args[i + 1]);

            if(args[i].equals("-q"))
                builder.withQuestionsFilePath(args[i + 1]);
        }

        return builder.build();
    }
}
